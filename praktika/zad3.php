<?php
$search = '';
$htmlBody = '';
if(isset($_POST['search']) && $_POST['search'] != ''){
    $search = htmlspecialchars($_POST['search']);//выражение для поиска
    $api_key = "AIzaSyCXtOJzaE-dimEkbN8EorTm9gbVIe5gd9g"; //youtube api key
    //Получаем последние добавленные 10 видео
    $json_result  = file_get_contents("https://www.googleapis.com/youtube/v3/search?part=id&order=date&q=".urlencode($search)."&type=video&key=".$api_key."&maxResults=10");
    $obj = json_decode($json_result);
    
    //Извлекаем ID видео
    $videoIDArr;
    foreach($obj->items as &$item) {
        $videoIDArr[] = $item->id->videoId;
    }
    $videoIDs = implode(',',$videoIDArr);
    
    //Получаем информацию по всем видео
    $json_result = file_get_contents('https://www.googleapis.com/youtube/v3/videos?id='.$videoIDs.'&key='.$api_key
        .'&fields=items(id,snippet(title,publishedAt),statistics)&part=snippet,statistics');
    $obj = json_decode($json_result);
    
    //Функция для сортировки по количеству просмотров
    function ViewSort($a, $b){
       if ($a->statistics->viewCount == $b->statistics->viewCount) return 0;
       return $a->statistics->viewCount > $b->statistics->viewCount ? -1 : 1;
    }
    
    //Функция для конвертации времени в нужный формат
    function ConvertTime($publishedTime){
        $d = new DateTime($publishedTime);
        return $d->format('H:i:s d-m');
    }
    
    //Сортируем массив по количеству просмотров
    usort($obj->items, 'ViewSort');
    
    //Формируем вывод
    $htmlBody = '<p>Поиск по запросу: '.$search.'</p>';
    foreach ($obj->items as &$item) {
        $htmlBody .= 'Просмотров: '.$item->statistics->viewCount.' Опубликовано: '.ConvertTime($item->snippet->publishedAt)
                    .' <a target="_blank" href="https://www.youtube.com/watch?v='.$item->id.'">'.$item->snippet->title.'</a><br/>';
    }
}
?>

<!doctype html>
<html>
    <head>
        <title>YouTube Search</title>
    </head>
    <body>
        <form action="/zad3.php" method="post">
          Поиск:
          <input type="text" name="search" value="<?=$search?>">
          <br><br>
          <input type="submit" value="Поиск">
        </form> 
        <?=$htmlBody?>
    </body>
</html>
